private static List<String> sort(List<String> list) {
    return list.stream().sorted().collect(Collector.toList());
}