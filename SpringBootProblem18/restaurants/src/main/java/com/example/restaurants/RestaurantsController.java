@RestController
@RequestMapping("/restaurants")
public class RestaurantsController {

    @Autowired
    private RestaurantsService restaurantsService;

    @GetMapping("/findTopFive")
    public ResponseEntity<Restaurants> findTopFive(){
        ResponseEntity.ok(restaurantsService.findTopFive());
    }
}
