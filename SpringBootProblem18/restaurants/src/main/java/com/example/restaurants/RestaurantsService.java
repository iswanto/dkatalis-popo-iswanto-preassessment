@Services
public class RestaurantsServices {

    @Autowired
    private RestaurantsRepository restaurantsRepository;

    public List<Restaurants> findTopFive()){
        return restaurantsRepository.getRestaurantsTopFive();
    }
}
