@Repository
public class RestaurantsServices {

	@Query("select * from restaurants order by name limit 5", native=true)
    public List<Restaurants> getRestaurantsTopFive();
}
