package com.example.restaurants;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestaurantsController {

	@InjectMocks
	private RestaurantsController restaurantsController;

	@Mock
	private RestaurantsService restaurantsService;

	@Test
	public void getTopFiveShouldReturnFiveValue() {
		 ResponseEntity<Restaurants> result = restaurantsController.findTopFive();
		 
		 List<Restaurants> restaurants = new ArrayList<Restaurants>;
		 restaurants.add(new Restaurants("a", "b", "c"));
		 restaurants.add(new Restaurants("a", "b", "c"));
		 restaurants.add(new Restaurants("a", "b", "c"));
		 restaurants.add(new Restaurants("a", "b", "c"));
		 restaurants.add(new Restaurants("a", "b", "c"));

		 Mockito.when(restaurantsService.findTopFive()).thenReturn();
		 Mockito.assertEquals(5, result size);
	}

}

