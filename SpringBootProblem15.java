public class SpringBootProblem15 {
    
    public int elapsedTime(String startTime, String endTime){
        LocalDateTime startTime = LocalDateTime.parse(startTime);
        LocalDateTime endTime = LocalDateTime.parse(endTime);
    
        int elapsedTime = endTime.compareTo(startTime); 
    }
}
